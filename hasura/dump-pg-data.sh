DATE=`date +%Y%m%d-%H%M`
BACK_DATA=illuminant-data-${DATE}.out
docker exec api_postgres_1 pg_dumpall -U postgres > ${BACK_DATA}

echo "docker cp ${BACK_DATA} api_postgres_1:/tmp" > restore-data.sh
echo "docker exec api_postgres_1 psql -U postgres -f /tmp/${BACK_DATA} postgres" >> restore-data.sh
