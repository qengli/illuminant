-- ----------------------------
-- Table structure for casbin_rule
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."casbin_rule";
CREATE TABLE "illuminant"."casbin_rule" (
  "p_type" text COLLATE "pg_catalog"."default" NOT NULL,
  "v0" text COLLATE "pg_catalog"."default" NOT NULL,
  "v1" text COLLATE "pg_catalog"."default" NOT NULL,
  "v2" text COLLATE "pg_catalog"."default" NOT NULL,
  "v3" text COLLATE "pg_catalog"."default" NOT NULL,
  "v4" text COLLATE "pg_catalog"."default" NOT NULL,
  "v5" text COLLATE "pg_catalog"."default" NOT NULL,
  "id" uuid NOT NULL DEFAULT gen_random_uuid()
)
;
COMMENT ON TABLE "illuminant"."casbin_rule" IS 'casbin策略';

-- ----------------------------
-- Table structure for dict
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."dict";
CREATE TABLE "illuminant"."dict" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "created_at" timestamptz(6) NOT NULL DEFAULT now(),
  "updated_at" timestamptz(6) NOT NULL DEFAULT now(),
  "catagory" text COLLATE "pg_catalog"."default" NOT NULL,
  "key" text COLLATE "pg_catalog"."default" NOT NULL,
  "val" text COLLATE "pg_catalog"."default" NOT NULL,
  "comment" text COLLATE "pg_catalog"."default"
)
;
COMMENT ON TABLE "illuminant"."dict" IS '字典表';

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."file";
CREATE TABLE "illuminant"."file" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "filename" text COLLATE "pg_catalog"."default" NOT NULL,
  "filepath" text COLLATE "pg_catalog"."default" NOT NULL
)
;
COMMENT ON TABLE "illuminant"."file" IS '文件表';

-- ----------------------------
-- Table structure for rel_role_user
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."rel_role_user";
CREATE TABLE "illuminant"."rel_role_user" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "role_id" uuid NOT NULL,
  "user_id" uuid NOT NULL
)
;
COMMENT ON TABLE "illuminant"."rel_role_user" IS '用户角色关联表';

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."resource";
CREATE TABLE "illuminant"."resource" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "created_at" timestamptz(6) NOT NULL DEFAULT now(),
  "updated_at" timestamptz(6) NOT NULL DEFAULT now(),
  "obj" text COLLATE "pg_catalog"."default" NOT NULL,
  "act" text COLLATE "pg_catalog"."default" NOT NULL,
  "comment" text COLLATE "pg_catalog"."default",
  "name" text COLLATE "pg_catalog"."default" NOT NULL
)
;
COMMENT ON TABLE "illuminant"."resource" IS '权限资源表';

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."role";
CREATE TABLE "illuminant"."role" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "created_at" timestamptz(6) NOT NULL DEFAULT now(),
  "updated_at" timestamptz(6) NOT NULL DEFAULT now(),
  "name" text COLLATE "pg_catalog"."default" NOT NULL,
  "comment" text COLLATE "pg_catalog"."default"
)
;
COMMENT ON TABLE "illuminant"."role" IS '角色表';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "illuminant"."user";
CREATE TABLE "illuminant"."user" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "created_at" timestamptz(6) NOT NULL DEFAULT now(),
  "updated_at" timestamptz(6) NOT NULL DEFAULT now(),
  "username" text COLLATE "pg_catalog"."default" NOT NULL,
  "password" text COLLATE "pg_catalog"."default" NOT NULL,
  "sex" int4 NOT NULL DEFAULT 1,
  "mobile" text COLLATE "pg_catalog"."default" NOT NULL,
  "email" text COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "illuminant"."user"."sex" IS '1-男;2-女';
COMMENT ON TABLE "illuminant"."user" IS '用户表';

-- ----------------------------
-- Function structure for armor
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."armor"(bytea);
CREATE OR REPLACE FUNCTION "illuminant"."armor"(bytea)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pg_armor'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for armor
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."armor"(bytea, _text, _text);
CREATE OR REPLACE FUNCTION "illuminant"."armor"(bytea, _text, _text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pg_armor'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for crypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."crypt"(text, text);
CREATE OR REPLACE FUNCTION "illuminant"."crypt"(text, text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pg_crypt'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for dearmor
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."dearmor"(text);
CREATE OR REPLACE FUNCTION "illuminant"."dearmor"(text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_dearmor'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for decrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."decrypt"(bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."decrypt"(bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_decrypt'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for decrypt_iv
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."decrypt_iv"(bytea, bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."decrypt_iv"(bytea, bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_decrypt_iv'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for digest
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."digest"(text, text);
CREATE OR REPLACE FUNCTION "illuminant"."digest"(text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_digest'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for digest
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."digest"(bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."digest"(bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_digest'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for encrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."encrypt"(bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."encrypt"(bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_encrypt'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for encrypt_iv
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."encrypt_iv"(bytea, bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."encrypt_iv"(bytea, bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_encrypt_iv'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for gen_random_bytes
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."gen_random_bytes"(int4);
CREATE OR REPLACE FUNCTION "illuminant"."gen_random_bytes"(int4)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_random_bytes'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for gen_random_uuid
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."gen_random_uuid"();
CREATE OR REPLACE FUNCTION "illuminant"."gen_random_uuid"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/pgcrypto', 'pg_random_uuid'
  LANGUAGE c VOLATILE
  COST 1;

-- ----------------------------
-- Function structure for gen_salt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."gen_salt"(text, int4);
CREATE OR REPLACE FUNCTION "illuminant"."gen_salt"(text, int4)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pg_gen_salt_rounds'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for gen_salt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."gen_salt"(text);
CREATE OR REPLACE FUNCTION "illuminant"."gen_salt"(text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pg_gen_salt'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for hmac
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."hmac"(bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."hmac"(bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_hmac'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for hmac
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."hmac"(text, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."hmac"(text, text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pg_hmac'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_armor_headers
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_armor_headers"(text, OUT "key" text, OUT "value" text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_armor_headers"(IN text, OUT "key" text, OUT "value" text)
  RETURNS SETOF "pg_catalog"."record" AS '$libdir/pgcrypto', 'pgp_armor_headers'
  LANGUAGE c IMMUTABLE STRICT
  COST 1
  ROWS 1000;

-- ----------------------------
-- Function structure for pgp_key_id
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_key_id"(bytea);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_key_id"(bytea)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pgp_key_id_w'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_decrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_decrypt"(bytea, bytea, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_decrypt"(bytea, bytea, text, text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pgp_pub_decrypt_text'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_decrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_decrypt"(bytea, bytea);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_decrypt"(bytea, bytea)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pgp_pub_decrypt_text'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_decrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_decrypt"(bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_decrypt"(bytea, bytea, text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pgp_pub_decrypt_text'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_decrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_decrypt_bytea"(bytea, bytea);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_decrypt_bytea"(bytea, bytea)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_decrypt_bytea'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_decrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_decrypt_bytea"(bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_decrypt_bytea"(bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_decrypt_bytea'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_decrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_decrypt_bytea"(bytea, bytea, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_decrypt_bytea"(bytea, bytea, text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_decrypt_bytea'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_encrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_encrypt"(text, bytea);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_encrypt"(text, bytea)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_encrypt_text'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_encrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_encrypt"(text, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_encrypt"(text, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_encrypt_text'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_encrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_encrypt_bytea"(bytea, bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_encrypt_bytea"(bytea, bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_encrypt_bytea'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_pub_encrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_pub_encrypt_bytea"(bytea, bytea);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_pub_encrypt_bytea"(bytea, bytea)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_pub_encrypt_bytea'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_decrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_decrypt"(bytea, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_decrypt"(bytea, text, text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pgp_sym_decrypt_text'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_decrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_decrypt"(bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_decrypt"(bytea, text)
  RETURNS "pg_catalog"."text" AS '$libdir/pgcrypto', 'pgp_sym_decrypt_text'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_decrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_decrypt_bytea"(bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_decrypt_bytea"(bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_sym_decrypt_bytea'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_decrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_decrypt_bytea"(bytea, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_decrypt_bytea"(bytea, text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_sym_decrypt_bytea'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_encrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_encrypt"(text, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_encrypt"(text, text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_sym_encrypt_text'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_encrypt
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_encrypt"(text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_encrypt"(text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_sym_encrypt_text'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_encrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_encrypt_bytea"(bytea, text, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_encrypt_bytea"(bytea, text, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_sym_encrypt_bytea'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for pgp_sym_encrypt_bytea
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."pgp_sym_encrypt_bytea"(bytea, text);
CREATE OR REPLACE FUNCTION "illuminant"."pgp_sym_encrypt_bytea"(bytea, text)
  RETURNS "pg_catalog"."bytea" AS '$libdir/pgcrypto', 'pgp_sym_encrypt_bytea'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for set_current_timestamp_updated_at
-- ----------------------------
DROP FUNCTION IF EXISTS "illuminant"."set_current_timestamp_updated_at"();
CREATE OR REPLACE FUNCTION "illuminant"."set_current_timestamp_updated_at"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Primary Key structure for table casbin_rule
-- ----------------------------
ALTER TABLE "illuminant"."casbin_rule" ADD CONSTRAINT "casbin_rule_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table dict
-- ----------------------------
CREATE TRIGGER "set_illuminant_dict_updated_at" BEFORE UPDATE ON "illuminant"."dict"
FOR EACH ROW
EXECUTE PROCEDURE "illuminant"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_illuminant_dict_updated_at" ON "illuminant"."dict" IS 'trigger to set value of column "updated_at" to current timestamp on row update';

-- ----------------------------
-- Primary Key structure for table dict
-- ----------------------------
ALTER TABLE "illuminant"."dict" ADD CONSTRAINT "dict_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table file
-- ----------------------------
ALTER TABLE "illuminant"."file" ADD CONSTRAINT "file_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table rel_role_user
-- ----------------------------
ALTER TABLE "illuminant"."rel_role_user" ADD CONSTRAINT "rel_role_user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table resource
-- ----------------------------
CREATE TRIGGER "set_illuminant_resource_updated_at" BEFORE UPDATE ON "illuminant"."resource"
FOR EACH ROW
EXECUTE PROCEDURE "illuminant"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_illuminant_resource_updated_at" ON "illuminant"."resource" IS 'trigger to set value of column "updated_at" to current timestamp on row update';

-- ----------------------------
-- Primary Key structure for table resource
-- ----------------------------
ALTER TABLE "illuminant"."resource" ADD CONSTRAINT "resource_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table role
-- ----------------------------
CREATE TRIGGER "set_illuminant_role_updated_at" BEFORE UPDATE ON "illuminant"."role"
FOR EACH ROW
EXECUTE PROCEDURE "illuminant"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_illuminant_role_updated_at" ON "illuminant"."role" IS 'trigger to set value of column "updated_at" to current timestamp on row update';

-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE "illuminant"."role" ADD CONSTRAINT "role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table user
-- ----------------------------
CREATE TRIGGER "set_illuminant_user_updated_at" BEFORE UPDATE ON "illuminant"."user"
FOR EACH ROW
EXECUTE PROCEDURE "illuminant"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_illuminant_user_updated_at" ON "illuminant"."user" IS 'trigger to set value of column "updated_at" to current timestamp on row update';

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "illuminant"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table rel_role_user
-- ----------------------------
ALTER TABLE "illuminant"."rel_role_user" ADD CONSTRAINT "rel_role_user_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "illuminant"."role" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "illuminant"."rel_role_user" ADD CONSTRAINT "rel_role_user_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "illuminant"."user" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
