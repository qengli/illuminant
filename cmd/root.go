package cmd

import (
	"illuminant/config"
	"illuminant/logger"

	"github.com/spf13/cobra"
)

var (
	cfgFile string
	rootCmd = &cobra.Command{
		Use:   "illuminant",
		Short: "illuminant command",
	}
)

func Execute() {
	rootCmd.Execute()
}

func init() {

	cobra.OnInitialize(initConfigAndLog)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file(default: ./settings.toml)")
	rootCmd.PersistentFlags().BoolP("version", "v", false, "illuminant version")

	rootCmd.AddCommand(cliCmd)
	rootCmd.AddCommand(servCmd)
}

func initConfigAndLog() {

	if err := config.InitConfig(cfgFile); err != nil {
		cobra.CheckErr(err)
	}

	cnf := config.Get()
	// init logger
	logger.InitLogger(cnf.Logger.Level, cnf.Logger.Dir, cnf.Logger.Filename,
		cnf.Logger.MaxSize, cnf.Logger.MaxBackups, cnf.Logger.MaxAge)
}
