package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var cliCmd = &cobra.Command{
	Use:   "cli name",
	Short: "cli command",
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Printf("hello %s", args[0])
		return nil
	},
}
