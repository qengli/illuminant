#!/bin/bash
#set -x
#******************************************************************************
# @file    : update-shema.sh
# @author  : wangyubin
# @date    : 2020-09-21 22:03:17
#
# @brief   : update schema from hasura
# history  : init
#******************************************************************************

host=106.54.141.54
port=9090

gq http://${host}:${port}/v1/graphql -H "X-Hasura-Admin-Secret: illuminantsecret" --introspect > schema.graphql

gqlgen generate

rm -f generated.go
