import ProForm from '@ant-design/pro-form';
import { formLayout } from '@/common';
import { Row, Col, Space } from 'antd';
import Upload from '@/components/UploadImage';

export default (props: any) => {
  return (
    <ProForm
      {...formLayout}
      layout="horizontal"
      onFinish={props.onFinish}
      submitter={{
        resetButtonProps: { style: { display: 'none' } },
        searchConfig: {
          submitText: '关闭',
        },
        render: (_, dom) => (
          <Row>
            <Col offset={10}>
              <Space>{dom}</Space>
            </Col>
          </Row>
        ),
      }}
    >
      <Upload title="选择图片" initFileList={[]} maxCount={2} />
    </ProForm>
  );
};
