import ProForm, { ProFormText, ProFormRadio } from '@ant-design/pro-form';
import { formLayout } from '@/common';
import { Row, Col, Space } from 'antd';

export default (props: any) => {
  return (
    <ProForm
      {...formLayout}
      layout="horizontal"
      onFinish={props.onFinish}
      initialValues={props.record}
      submitter={{
        // resetButtonProps: { style: { display: 'none' } },
        render: (_, dom) => (
          <Row>
            <Col offset={10}>
              <Space>{dom}</Space>
            </Col>
          </Row>
        ),
      }}
    >
      <ProFormText
        name="username"
        label="姓名"
        rules={[{ required: true, message: '请输入姓名' }]}
      />
      <ProFormText
        name="mobile"
        label="手机号码"
        rules={[{ required: true, message: '请输入手机号码' }]}
      />
      <ProFormText name="email" label="邮箱" />
      <ProFormRadio.Group
        name="sex"
        label="性别"
        options={[
          { label: '男', value: 1 },
          { label: '女', value: 2 },
        ]}
      />
    </ProForm>
  );
};
