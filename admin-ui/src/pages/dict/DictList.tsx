import { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Modal, Popconfirm, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getDictList, createDict, updateDict, deleteDict } from '@/services/dict/api';
import DictAdd from './DictAdd';
import DictEdit from './DictEdit';
import styles from '@/pages/style.less';

export default () => {
  const tableRef = useRef<ActionType>();
  const [modalAddVisible, setModalAddVisible] = useState(false);
  const [modalEditVisible, setModalEditVisible] = useState(false);
  const [record, setRecord] = useState<API.DictItem>({});
  const [selectedRowId, setSelectedRowId] = useState('');

  const getRowClassName = (rd: API.DictItem) => {
    if ((rd.id as string) === selectedRowId) return styles.selectedRow;
    return '';
  };

  const columns: ProColumns<API.DictItem>[] = [
    {
      title: '类别',
      dataIndex: 'catagory',
    },
    {
      title: '键',
      dataIndex: 'key',
    },
    {
      title: '值',
      dataIndex: 'val',
    },
    {
      title: '备注',
      dataIndex: 'comment',
      hideInSearch: true,
    },
    {
      title: '操作',
      valueType: 'option',
      render: (_, rd) => [
        <Button
          type="primary"
          size="small"
          key="edit"
          onClick={() => {
            setModalEditVisible(true);
            setRecord(rd);
          }}
        >
          修改
        </Button>,
        <Popconfirm
          placement="topRight"
          title="是否删除?"
          okText="Yes"
          cancelText="No"
          key="delete"
          onConfirm={async () => {
            const response = await deleteDict(rd.id as string);
            if (response.code === 10000) message.info(`字典: 【${rd.key}】 删除成功`);
            else message.warn(`字典: 【${rd.key}】 删除失败`);
            tableRef.current?.reload();
          }}
        >
          <Button danger size="small">
            删除
          </Button>
        </Popconfirm>,
      ],
    },
  ];

  const addItem = async (values: any) => {
    console.log(values);
    const response = await createDict(values);
    if (response.code !== 10000) {
      message.error('创建字典失败');
    }

    if (response.code === 10000) {
      setModalAddVisible(false);
      tableRef.current?.reload();
      setSelectedRowId(response.data.insert_illuminant_dict_one.id);
    }
  };

  const editItem = async (values: any) => {
    values.id = record.id;
    console.log(values);
    const response = await updateDict(values);
    if (response.code !== 10000) {
      message.error('编辑字典失败');
    }

    if (response.code === 10000) {
      setModalEditVisible(false);
      tableRef.current?.reload();
      setSelectedRowId(response.data.update_illuminant_dict_by_pk.id);
    }
  };

  return (
    <PageContainer fixedHeader header={{ title: '字典列表' }}>
      <ProTable<API.DictItem>
        columns={columns}
        rowClassName={getRowClassName}
        rowKey="id"
        actionRef={tableRef}
        search={{
          labelWidth: 'auto',
        }}
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              setModalAddVisible(true);
            }}
          >
            新建
          </Button>,
        ]}
        request={async (params: API.DictItem & API.PageInfo) => {
          const resp = await getDictList(params);
          return {
            data: resp.data.illuminant_dict,
            total: resp.data.illuminant_dict_aggregate.aggregate.count,
          };
        }}
      />
      <Modal
        destroyOnClose
        title="新增"
        visible={modalAddVisible}
        footer={null}
        onCancel={() => setModalAddVisible(false)}
      >
        <DictAdd onFinish={addItem} />
      </Modal>
      <Modal
        destroyOnClose
        title="编辑"
        visible={modalEditVisible}
        footer={null}
        onCancel={() => setModalEditVisible(false)}
      >
        <DictEdit onFinish={editItem} record={record} />
      </Modal>
    </PageContainer>
  );
};
