import ProForm, { ProFormText } from '@ant-design/pro-form';
import { formLayout } from '@/common';
import { Row, Col, Space } from 'antd';

export default (props: any) => {
  return (
    <ProForm
      {...formLayout}
      layout="horizontal"
      onFinish={props.onFinish}
      submitter={{
        // resetButtonProps: { style: { display: 'none' } },
        render: (_, dom) => (
          <Row>
            <Col offset={10}>
              <Space>{dom}</Space>
            </Col>
          </Row>
        ),
      }}
    >
      <ProFormText
        name="catagory"
        label="类别"
        rules={[{ required: true, message: '请输入类别' }]}
      />
      <ProFormText
        name="key"
        label="键"
        rules={[{ required: true, message: '请输入字典的键' }]}
      />
      <ProFormText
        name="val"
        label="值"
        rules={[{ required: true, message: '请输入字典的值' }]}
      />
      <ProFormText
        name="comment"
        label="备注"
      />
    </ProForm>
  );
};