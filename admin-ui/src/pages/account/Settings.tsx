import ProForm, { ProFormText } from '@ant-design/pro-form';
import { PageContainer } from '@ant-design/pro-layout';
import { Row, Col, Space, Card, message } from 'antd';
import { changePasswd } from '@/services/user/api';

export default () => {
  const formLayout = {
    labelCol: {
      span: 2,
    },
    wrapperCol: {
      span: 5,
    },
  };

  const onFinish = async (values: any) => {
    if (values.newpasswd2 !== values.newpasswd) {
      message.error('两次输入的新密码不一致');
      return;
    }

    const response = await changePasswd(values.oldpasswd, values.newpasswd);
    if (response.code === 10000) {
      message.success(response.message);
    } else {
      message.error(response.message);
    }
  };

  return (
    <PageContainer fixedHeader header={{ title: '密码修改' }}>
      <Card>
        <ProForm
          {...formLayout}
          layout="horizontal"
          onFinish={onFinish}
          submitter={{
            // resetButtonProps: { style: { display: 'none' } },
            render: (_, dom) => (
              <Row>
                <Col offset={5}>
                  <Space>{dom}</Space>
                </Col>
              </Row>
            ),
          }}
        >
          <ProFormText.Password
            name="oldpasswd"
            label="旧密码"
            rules={[{ required: true, message: '请输入旧密码' }]}
          />
          <ProFormText.Password
            name="newpasswd"
            label="新密码"
            rules={[{ required: true, message: '请输入新密码' }]}
          />
          <ProFormText.Password
            name="newpasswd2"
            label="再次输入新密码"
            rules={[{ required: true, message: '再次输入新密码' }]}
          />
        </ProForm>
      </Card>
    </PageContainer>
  );
};
