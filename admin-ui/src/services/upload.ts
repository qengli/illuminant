// @ts-ignore
/* eslint-disable */
import { RestPost, RestGet } from './utils';

export async function UploadFile(type: string, file: any) {
  const formData = new FormData();
  formData.append('file', file);

  return RestPost<API.UploadResponse>(`/api/v1/file/upload/${type}`, formData);
}

export async function DeleteFile(id: string) {
  return RestPost<API.UploadResponse>(`/api/v1/file/delete/${id}`, null);
}

export async function RefreshFileStatus(id: string) {
  return RestPost<API.UploadResponse>(`/api/v1/file/refresh/${id}`, null);
}

export async function DownloadFile(id: string, filename: string) {
  return RestGet(`/api/v1/file/download/${id}`).then((res) => {
    let url = URL.createObjectURL(new Blob([res as BlobPart]));
    let a = document.createElement('a');
    a.href = url;
    a.download = filename;
    a.click();
    URL.revokeObjectURL(url);
  });
}
