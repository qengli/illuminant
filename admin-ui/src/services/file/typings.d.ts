// @ts-ignore
/* eslint-disable */

declare namespace API {
  type FileItem = {
    id?: string;
    filename?: string;
    filepath?: string;
    status?: number;
  };

  type FileListResult = CommonResponse & {
    data: {
      illuminant_file: FileItem[];
      illuminant_file_aggregate: {
        aggregate: {
          count: number;
        };
      };
    };
  };

  type CreateFileResult = CommonResponse & {
    data: {
      insert_illuminant_file: {
        affected_rows: number;
      };
    };
  };

  type UpdateFileResult = CommonResponse & {
    data: {
      update_illuminant_file_by_pk: {
        id: string;
      };
    };
  };

  type DeleteFileResult = CommonResponse & {
    data: {
      delete_illuminant_file_by_pk: {
        id: string;
      };
    };
  };
}