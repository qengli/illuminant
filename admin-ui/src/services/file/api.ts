// @ts-ignore
/* eslint-disable */
import { Graphql } from '../utils';

const gqlGetFileList = `query get_item_list($limit: Int = 10, $offset: Int = 0, $filename: String, $filepath: String, $status: [Int!] = [0,1]) {
  illuminant_file(order_by: {updated_at: desc}, limit: $limit, offset: $offset, where: {filename: {_ilike: $filename}, filepath: {_ilike: $filepath}, status: {_in: $status}}) {
    id
    filename
    filepath
    status
  }
  illuminant_file_aggregate(where: {filename: {_ilike: $filename}, filepath: {_ilike: $filepath}, status: {_in: $status}}) {
    aggregate {
      count
    }
  }
}`;

const gqlCreateFile = `mutation create_item($filename: String!, $filepath: String!, $status: Int = 1) {
  insert_illuminant_file(objects: { filename: $filename, filepath: $filepath, status: $status }) {
    affected_rows
  }
}`;

const gqlUpdateFile = `mutation update_item_by_pk($id: uuid!, $filename: String, $filepath: String, $status: Int) {
  update_illuminant_file_by_pk(pk_columns: {id: $id}, _set: { filename: $filename, filepath: $filepath, status: $status }) {
    id
  }
}`;

const gqlDeleteFile = `mutation delete_item_by_pk($id: uuid!) {
  delete_illuminant_file_by_pk(id: $id) {
    id
  }
}`;

export async function getFileList(params: API.FileItem & API.PageInfo) {
  const gqlVar: any = {
    limit: params.pageSize ? params.pageSize : 10,
    offset: params.current && params.pageSize ? (params.current - 1) * params.pageSize : 0,
    filename: params.filename ? '%' + params.filename + '%' : '%%',
    filepath: params.filepath ? '%' + params.filepath + '%' : '%%',
  };

  if (params.status) gqlVar.status = [params.status];

  return Graphql<API.FileListResult>(gqlGetFileList, gqlVar);
}

export async function createFile(params: API.FileItem) {
  const gqlVar = {
    filename: params.filename,
    filepath: params.filepath,
  };

  return Graphql<API.CreateFileResult>(gqlCreateFile, gqlVar);
}

export async function updateFile(params: API.FileItem) {
  const gqlVar = {
    id: params.id,
    filename: params.filename,
    filepath: params.filepath,
    status: params.status ? params.status : null,
  };

  return Graphql<API.UpdateFileResult>(gqlUpdateFile, gqlVar);
}

export async function deleteFile(id: string) {
  return Graphql<API.DeleteFileResult>(gqlDeleteFile, { id });
}
