// @ts-ignore
/* eslint-disable */

declare namespace API {
  type CurrentUser = {
    username: string;
    avatar?: string;
    email?: string;
    mobile: string;
  };

  type LoginResult = CommonResponse & {
    type?: string;
    data: {
      token: string;
      expire?: string;
      status: number;
    };
  };

  type FakeCaptcha = {
    code?: number;
    status?: string;
  };

  type LoginParams = {
    mobile: string;
    password: string;
    type?: string;
  };

  type ChangePasswdResult = CommonResponse & {};
}
