// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { RestGet, RestPost } from '../utils';
import md5 from 'md5';

/** 获取当前的用户 GET /api/v1/currentUser */
export async function currentUser() {
  return RestGet<{ data: API.CurrentUser }>('/api/v1/current_user');
}

/** 退出登录 */
export function outLogin() {
  window.localStorage.clear();
}

/** 登录接口 POST /api/v1/login */
export async function login(body: API.LoginParams, options?: { [key: string]: any }) {
  return request<API.LoginResult>('/api/v1/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 修改密码 **/
export async function changePasswd(old_passwd: string, new_passwd: string) {
  return RestPost<API.ChangePasswdResult>('/api/v1/account/changepasswd', {
    old_passwd: md5(old_passwd),
    new_passwd: md5(new_passwd),
  });
}
