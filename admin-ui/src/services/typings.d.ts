// @ts-ignore
/* eslint-disable */

declare namespace API {
  type CommonResponse = {
    code: number;
    message: string;
  };

  type PageInfo = {
    pageSize?: number;
    current?: number;
  };

  type ErrorResponse = {
    /** 业务约定的错误码 */
    errorCode: string;
    /** 业务上的错误信息 */
    errorMessage?: string;
    /** 业务上的请求是否成功 */
    success?: boolean;
  };

  type UploadFileInfo = {
    uid?: string; // 文件唯一标识
    name: string; // 文件名
    status?: string; // 文件状态 uploading done error removed
    url?: string; // 下载地址
    thumbUrl?: string; //缩略图地址
  };

  type UploadResponse = CommonResponse & {
    data: {
      fid: string; // illuminant_file 表的id
    };
  };
}
