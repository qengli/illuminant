// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

export async function RestGet<T>(url: string) {
  return request<T>(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export async function RestPost<T>(url: string, body: any) {
  return request<T>(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
  });
}

export async function Graphql<T>(query: string, variables: any) {
  return request<T>('/api/v1/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: {
      query,
      variables,
    },
  });
}
