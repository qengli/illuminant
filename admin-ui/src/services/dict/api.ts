// @ts-ignore
/* eslint-disable */
import { Graphql } from '../utils';

const gqlGetDictList = `query get_item_list($limit: Int = 10, $offset: Int = 0, $catagory: String, $key: String, $val: String) {
  illuminant_dict(order_by: {updated_at: desc}, limit: $limit, offset: $offset, where: {catagory: {_ilike: $catagory}, key: {_ilike: $key}, val: {_ilike: $val}}) {
    id
    catagory
    key
    val
    comment
  }
  illuminant_dict_aggregate(where: {catagory: {_ilike: $catagory}, key: {_ilike: $key}, val: {_ilike: $val}}) {
    aggregate {
      count
    }
  }
}`;

const gqlCreateDict = `mutation create_item($catagory: String!, $key: String!, $val: String!, $comment: String) {
  insert_illuminant_dict_one(object: {catagory: $catagory, key: $key, val: $val, comment: $comment}) {
    id
  }
}`;

const gqlUpdateDict = `mutation update_item_by_pk($id: uuid!, $catagory: String, $key: String, $val: String, $comment: String) {
  update_illuminant_dict_by_pk(pk_columns: {id: $id}, _set: { catagory: $catagory, key: $key, val: $val, comment: $comment }) {
    id
  }
}`;

const gqlDeleteDict = `mutation delete_item_by_pk($id: uuid!) {
  delete_illuminant_dict_by_pk(id: $id) {
    id
  }
}`;

export async function getDictList(params: API.DictItem & API.PageInfo) {
  const gqlVar = {
    limit: params.pageSize ? params.pageSize : 10,
    offset: params.current && params.pageSize ? (params.current - 1) * params.pageSize : 0,
    catagory: params.catagory ? '%' + params.catagory + '%' : '%%',
    key: params.key ? '%' + params.key + '%' : '%%',
    val: params.val ? '%' + params.val + '%' : '%%',
  };

  return Graphql<API.DictListResult>(gqlGetDictList, gqlVar);
}

export async function createDict(params: API.DictItem) {
  const gqlVar = {
    catagory: params.catagory,
    key: params.key,
    val: params.val,
    comment: params.comment ? params.comment : null,
  };

  return Graphql<API.CreateDictResult>(gqlCreateDict, gqlVar);
}

export async function updateDict(params: API.DictItem) {
  const gqlVar = {
    id: params.id,
    catagory: params.catagory,
    key: params.key,
    val: params.val,
    comment: params.comment ? params.comment : null,
  };

  return Graphql<API.UpdateDictResult>(gqlUpdateDict, gqlVar);
}

export async function deleteDict(id: string) {
  return Graphql<API.DeleteDictResult>(gqlDeleteDict, { id });
}
