// @ts-ignore
/* eslint-disable */

declare namespace API {
  type DictItem = {
    id?: string;
    catagory?: string;
    key?: string;
    val?: string;
    comment?: string;
  };

  type DictListResult = CommonResponse & {
    data: {
      illuminant_dict: DictItem[];
      illuminant_dict_aggregate: {
        aggregate: {
          count: number;
        };
      };
    };
  };

  type CreateDictResult = CommonResponse & {
    data: {
      insert_illuminant_dict_one: {
        id: string;
      };
    };
  };

  type UpdateDictResult = CommonResponse & {
    data: {
      update_illuminant_dict_by_pk: {
        id: string;
      };
    };
  };

  type DeleteDictResult = CommonResponse & {
    data: {
      delete_illuminant_dict_by_pk: {
        id: string;
      };
    };
  };
}
