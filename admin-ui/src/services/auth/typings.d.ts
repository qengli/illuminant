// @ts-ignore
/* eslint-disable */

declare namespace API {
  type UserItem = {
    id?: string;
    username?: string;
    sex?: number;
    mobile?: string;
    email?: string;
    created_at?: string;
    updated_at?: string;
  };

  type UserListResult = CommonResponse & {
    data: {
      illuminant_user: UserItem[];
      illuminant_user_aggregate: {
        aggregate: {
          count: number;
        };
      };
    };
  };

  type CreateUserResult = CommonResponse & {
    data: {
      insert_illuminant_user_one: {
        id: string;
      };
    };
  };

  type UpdateUserResult = CommonResponse & {
    data: {
      update_illuminant_user_by_pk: {
        id: string;
      };
    };
  };

  type DeleteUserResult = CommonResponse & {
    data: {
      delete_illuminant_user_by_pk: {
        id: string;
      };
    };
  };
}
