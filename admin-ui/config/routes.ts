﻿export default [
  {
    path: '/account',
    routes: [
      {
        path: '/account',
        routes: [
          {
            name: 'login',
            layout: false,
            path: '/account/login',
            component: './account/Login',
          },
          {
            name: 'settings',
            path: '/account/settings',
            component: './account/Settings',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: 'auth',
    name: 'auth',
    icon: 'team',
    routes: [
      {
        path: '/auth/user',
        name: 'user',
        icon: 'user',
        component: './auth/user/UserList',
      },
    ],
  },
  {
    path: '/dict',
    name: 'dict',
    icon: 'read',
    component: './dict/DictList',
  },
  {
    path: '/file',
    name: 'file',
    icon: 'file',
    component: './file/FileList',
  },
  {
    path: '/',
    redirect: '/auth/user',
  },
  {
    component: './404',
  },
];
