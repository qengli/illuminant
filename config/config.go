package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	Server  serverConfig  `toml:"server"`
	Auth    authConfig    `toml:"auth"`
	Graphql GraphqlConfig `toml:"graphql"`
	Logger  loggerConfig  `toml:"logger"`
	File    fileConfig    `toml:"file"`
}

type serverConfig struct {
	Port int `toml:"port"`
}

type authConfig struct {
	RBACModel string `toml:"rbac_model"`
}

type GraphqlConfig struct {
	Endpoint    string `toml:"endpoint"`
	Secret      string `toml:"secret"`
	PGDumpURL   string `toml:"pg_dump_url"`
	MetaDataURL string `toml:"meta_data_url"`
}

type loggerConfig struct {
	Level      string `toml:"level"`
	Dir        string `toml:"dir"`
	Filename   string `toml:"filename"`
	MaxSize    int    `toml:"max_size"`
	MaxBackups int    `toml:"max_backups"`
	MaxAge     int    `toml:"max_age"`
}

type fileConfig struct {
	UploadDir   string `toml:"upload_dir"`
	DownloadDir string `toml:"download_dir"`
	StaticDir   string `toml:"static_dir"`
}

var conf *Config

func Get() *Config {
	return conf
}

func InitConfig(confPath string) error {
	if confPath == "" {
		confPath = "./settings.toml"
	}
	viper.SetConfigFile(confPath)
	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	return viper.Unmarshal(&conf)
}
