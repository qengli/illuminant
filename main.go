package main

import (
	"illuminant/cmd"
)

func main() {
	cmd.Execute()
}
