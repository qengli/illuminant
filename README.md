# illuminant

#### 文档
https://www.yuque.com/quyuan-w1et4/awzlgk

#### 概述
高效的后端开发模板, 通过 graphql 自动化生成大部分API

更多的介绍可参见: [高效的后端API开发模板-illuminant](https://www.cnblogs.com/wang_yb/p/12742872.html "illuminant")

#### 环境准备

1. golang (https://golang.org/dl/)
2. postgresql(docker)
```bash
docker pull postgres:12
```
3. graphql-engine(docker)
```bash
docker pull hasura/graphql-engine:v2.0.10
```

#### 简介

目前, 本工程主要是一个后端工程的模板, 在 /route.go 中有各类示例用的API(包括login, upload, download, graphql等等),

后续准备完善认证, 权限相关内容, 以及管理后台的页面

整体及illuminant内部结构

![arch](./arch.png)

```bash
# 下载代码
git clone https://gitee.com/wangyubin/illuminant.git

# 安装golang的依赖
cd illuminant
go mod tidy  # 安装依赖包

# 直接从脚本安装
curl https://gitee.com/wangyubin/illuminant/raw/dev/create-project.sh | /bin/bash -s YOU_PROJECT_NAME
```

#### 访问
启动之后, 默认在 `9999` 端口提供API服务.

后端主要嫁接了hasura 的接口, 具体可以参照: https://hasura.io/

