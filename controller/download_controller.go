package controller

import (
	"illuminant/service"
	"illuminant/util"

	"github.com/gin-gonic/gin"
)

func DownloadFile(c *gin.Context) {
	fileId := c.Param("fileId")
	file, err := service.GetFileByID(fileId)
	if err != nil {
		util.Fail(c, util.DOWNLOAD_ERROR, err.Error(), nil)
		return
	}

	c.File(file.Filepath)
}
