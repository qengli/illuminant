#!/bin/bash
#set -x
#******************************************************************************
# @file    : create-project.sh
# @author  : wangyubin
# @date    : 2021-01-11 12:16:49
#
# @brief   : create project base on illuminant
# history  : init
#******************************************************************************

if [ "$#" -ne 1 ]; then
    echo "You SHOULD input project name"
    exit 1
fi

git clone --depth 1 git@gitee.com:wangyubin/illuminant.git
rm -rf ./illuminant/.git
mv ./illuminant $1
cd $1

# 替换引用的package
sed -i "s/\"illuminant\//\"$1\//g" `grep '"illuminant/' -rl .`

# 替换go.mod中的模块名
sed -i "s/module illuminant/module $1/g" go.mod

# 替换 .gitignore 中的模块名
sed -i "s/illuminant/$1/g" .gitignore

