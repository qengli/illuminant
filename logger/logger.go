package logger

import (
	"log"

	"gitee.com/wangyubin/gutils/logger"
	"github.com/rs/zerolog"
)

var myLogger *zerolog.Logger

func InitLogger(level, dir, filename string, maxSize, maxBackups, maxAge int) {
	l := logger.Debug

	switch level {
	case "info":
		l = logger.Info
	case "warn":
		l = logger.Warn
	case "error":
		l = logger.Error
	case "fatal":
		l = logger.Fatal
	default:
	}

	myLogger = logger.NewLogger(logger.LogConfig{
		ConsoleEnabled: true,
		FileEnabled:    true,
		Level:          l,
		LogDir:         dir,
		FileName:       filename,
		MaxSize:        maxSize,
		MaxAge:         maxAge,
		MaxBackups:     maxBackups,
	})

}

func GetLogger() *zerolog.Logger {
	if myLogger == nil {
		log.Fatal("You must init logger first!")
	}
	return myLogger
}
