package routes

import (
	"illuminant/controller"
	"illuminant/util"

	"github.com/gin-gonic/gin"
)

func adminRoutes(r *gin.RouterGroup) {
	a := r.Group("/admin")

	// 权限策略管理API
	a.GET("/authority/permissions/:role", controller.GetRolePermissions)
	a.POST("/authority/permission/add", controller.AddRolePermission)
	a.POST("/authority/permission/delete", controller.DeleteRolePermission)
	a.POST("/authority/user/roles/add", controller.AddRolesForUser)
	a.POST("/authority/user/roles/delete", controller.DeleteRolesForUser)
	a.POST("/authority/user/role/delete", controller.DeleteRoleForUser)

	a.POST("/graphql", util.ReverseProxy())

	// 数据库结果导出 API
	a.POST("/maintain/pg-dump", controller.PGDumpSchema)
	// hasura meta data 导出API
	a.POST("/maintain/hasura/meta-data", controller.HasuraMetaData)
}
