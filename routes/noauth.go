package routes

import (
	"illuminant/middleware"

	"github.com/gin-gonic/gin"
)

func noAuthRoutes(r *gin.RouterGroup) {
	authMiddleware := middleware.JwtMiddleware()

	r.POST("/login", authMiddleware.LoginHandler)
	r.GET("/refresh_token", authMiddleware.RefreshHandler)
}
